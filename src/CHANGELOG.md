* 0.1.1
  * Support for generic impurity measures (_e.g._ Gini)

* 0.1.0
  * Initial version