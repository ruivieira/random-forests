![https://crates.io/crates/randomforests](https://img.shields.io/crates/v/randomforests.svg) ![https://docs.rs/randomforests/](https://docs.rs/randomforests/badge.svg) ![](https://gitlab.com/ruivieira/random-forests/badges/master/build.svg
)

# random-forests

A Rust library for [Random Forests](https://en.wikipedia.org/wiki/Random_forest).

Support for generic impurity measures:

* [x] Entropy
* [x] Gini

## installation

Add to your `Cargo.toml`:

```text
randomforests = "*"
```

## usage

Add the crate and `RandomForests` to your code:

```rust
extern crate randomforests;

use randomforests::RandomForest;
```

Create a `Dataset` as collection of `Item`s:

```rust
let mut dataset = Dataset::new();
let mut item1 = Item::new();
item1.insert("lang".to_string(), Value { data: "rust".to_string() });
item1.insert("typing".to_string(), Value { data: "static".to_string() });
dataset.push(item1);

let mut item2 = Item::new();
item2.insert("lang".to_string(), Value { data: "python".to_string() } );
item2.insert("typing".to_string(), Value { data: "dynamic".to_string() } );
dataset.push(item2);

let mut item3 = Item::new();
item3.insert("lang".to_string(), Value { data: "haskell".to_string() });
item3.insert("typing".to_string(), Value { data: "static".to_string() });
dataset.push(item3);
```

Initialise the classifier and train it classifier by passing the `Dataset`, a `TreeConfig`, the number of trees and the data subsample size: 

```rust
let mut config = TreeConfig::new();
config.decision = "lang".to_string();
let forest = RandomForest::build("lang".to_string(), config, &dataset, 100, 3);
```

Create a question as an `Item`:

```rust
let mut question = Item::new();
question.insert("typing".to_string(), Value { data: "static".to_string() });
```

And get the predictions:

```rust
let answer = RandomForest::predict(forest, question);
// answer = {Value { data: haskell }: 48, Value { data: rust }: 52}
```